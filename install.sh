#!/bin/bash
cp vimrc ~/.vimrc
cp bashrc ~/.bashrc
cp inputrc ~/.inputrc
cp tmux.conf ~/.tmux.conf
